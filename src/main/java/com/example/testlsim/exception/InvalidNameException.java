package com.example.testlsim.exception;

public class InvalidNameException extends RuntimeException {
    private final String message;

    public InvalidNameException(String message) {
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }
}
