package com.example.testlsim.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Test {
    private Long id;
    private String name;
    private Date done_date;
    private Integer status;
}
