package com.example.testlsim.controller.exceptioncontroller;

import com.example.testlsim.controller.ApiController;
import com.example.testlsim.exception.InvalidNameException;
import com.example.testlsim.exception.NameIsNullException;
import com.example.testlsim.response.ErrorDto;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Log4j2
public class Handle {
    static Logger logger = LogManager.getLogger(ApiController.class);


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorDto handleInternalServerErrors(Exception ex) {
        logger.error("InternalServerError: {}", ex);
        return new ErrorDto(
                HttpStatus.INTERNAL_SERVER_ERROR.name(),
                ex.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NameIsNullException.class)
    public ErrorDto handleNameIsNull(NameIsNullException ex) {
        logger.error("NameIsNull: {}", ex);
        return new ErrorDto(
                HttpStatus.BAD_REQUEST.name(),
                ex.getMessage());
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidNameException.class)
    public ErrorDto handleInvalidName(InvalidNameException ex) {
        logger.error("InvalidName: {}", ex);
        return new ErrorDto(
                HttpStatus.BAD_REQUEST.name(),
                ex.getMessage());
    }

}
