package com.example.testlsim.controller;

import com.example.testlsim.entity.Test;
import com.example.testlsim.response.RespTest;
import com.example.testlsim.service.TestService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {
    static Logger logger = LogManager.getLogger(ApiController.class);

    @Autowired
    private TestService testService;

    @GetMapping("/getTest")
    List<RespTest> getTestList(){
        logger.info("Get test list");
     return testService.getTestList();
    }

    @GetMapping("/getTestByName")
    public RespTest getTestByName(@RequestParam String name){
        logger.info("Get test by name"+name);
        return testService.getTestByName(name);
    }



}
