package com.example.testlsim.response;

import lombok.Data;

import java.util.Date;

@Data
public class RespTest {
    private String name;
    private Date done_date;
    private Integer status;
}
