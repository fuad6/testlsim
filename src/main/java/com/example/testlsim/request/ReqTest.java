package com.example.testlsim.request;

import lombok.Data;

import java.util.Date;

@Data
public class ReqTest {
    private String name;
    private Date done_date;
}
