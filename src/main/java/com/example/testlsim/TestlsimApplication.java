package com.example.testlsim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestlsimApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestlsimApplication.class, args);
    }

}
