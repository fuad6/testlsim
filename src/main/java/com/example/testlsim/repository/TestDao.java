package com.example.testlsim.repository;

import com.example.testlsim.entity.Test;

import java.util.List;
import java.util.Optional;

public interface TestDao {
    List<Test> getList() throws Exception;
    Test getTestByName(String name) ;
    void updateTestByName(String name) ;


}
