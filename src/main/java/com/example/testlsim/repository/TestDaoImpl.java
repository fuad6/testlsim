package com.example.testlsim.repository;

import com.example.testlsim.entity.Test;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Repository
public class TestDaoImpl implements TestDao {

  private  DataSource dataSource;

    @Override
    public List<Test> getList() {
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        String sql="Select * from test where status=1";
        List<Test> testList=jdbcTemplate.query(sql,new BeanPropertyRowMapper(Test.class));
        return testList;
    }

    @Override
    public Test getTestByName(String name) {
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        String sql="select * from test where name=?";
       return (Test) jdbcTemplate.queryForObject(sql,new Object[]{name},new BeanPropertyRowMapper(Test.class));
    }

    @Override
    public void updateTestByName(String name) {
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        String sql="update test set status=0  where name=?";
          jdbcTemplate.update(sql,new Object[]{name});
    }


}
