package com.example.testlsim.service;

import com.example.testlsim.exception.InvalidNameException;
import com.example.testlsim.exception.NameIsNullException;
import com.example.testlsim.entity.Test;


import com.example.testlsim.repository.TestDao;
import com.example.testlsim.response.RespTest;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Log4j2
@AllArgsConstructor
@Service
public class TestServiceImpl implements TestService {
    private static final Logger logger = LogManager.getLogger(TestServiceImpl.class);

    TestDao testDao;

    @Override
    public List<RespTest> getTestList() {
        List<RespTest> respTestList = new ArrayList<>();

        try {
            List<Test> testList = testDao.getList();
            logger.info("getTestList");
//            log.info("getTestList: {}", testList);
//            testlist.forEach(test -> {
//                        RespTest respTest=new RespTest();
//                        respTest.setName(test.getName());
//                        respTest.setDone_date(test.getDone_date());
//                        respTestList.add(respTest);
//                    });
            for (Test test : testList) {
                RespTest respTest = new RespTest();
                respTest.setName(test.getName());
                respTest.setDone_date(test.getDone_date());
                respTest.setStatus(test.getStatus());
                respTestList.add(respTest);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return respTestList;
    }

    @Override
    public RespTest getTestByName(String name) {
        RespTest respTest = new RespTest();
        logger.info("Name: {}", name);
        if (Strings.isBlank(name)) {
            throw new NameIsNullException("name is null");
        }
        logger.info("InvalidName: {}", name);


        Test test = testDao.getTestByName(name);
        Optional<Test> test1 = Optional.of(test);
        if(test1.isPresent()){
            testDao.updateTestByName(name);
            logger.info("updated status");
            Test test2 = testDao.getTestByName(name);
            respTest.setName(test2.getName());
            respTest.setDone_date(test2.getDone_date());
            respTest.setStatus(test2.getStatus());
        }
        else{
            throw  new InvalidNameException("there was no such name");
        }


//        if(!name.equals(respTest.getName())){
//
//        }
        return respTest;
    }



}
