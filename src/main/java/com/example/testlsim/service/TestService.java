package com.example.testlsim.service;

import com.example.testlsim.response.RespTest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface TestService {
    List<RespTest> getTestList();
    RespTest getTestByName(String name);


}
